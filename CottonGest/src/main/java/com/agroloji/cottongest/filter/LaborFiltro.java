package com.agroloji.cottongest.filter;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.agroloji.cottongest.entity.Parcela;

import lombok.Data;

@Data
public class LaborFiltro {

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date fecha;
	
	private Parcela parcela;
	

}
