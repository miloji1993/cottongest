package com.agroloji.cottongest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CottonGestApplication {

	public static void main(String[] args) {
		SpringApplication.run(CottonGestApplication.class, args);
	}

}
