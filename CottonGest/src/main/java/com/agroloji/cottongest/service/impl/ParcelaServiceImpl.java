package com.agroloji.cottongest.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.agroloji.cottongest.dto.ParcelaDto;
import com.agroloji.cottongest.entity.Cliente;
import com.agroloji.cottongest.entity.Parcela;
import com.agroloji.cottongest.mapper.ParcelaMapper;
import com.agroloji.cottongest.repository.ClienteRepository;
import com.agroloji.cottongest.repository.ParcelaRepository;
import com.agroloji.cottongest.service.ParcelaService;

@Service
public class ParcelaServiceImpl implements ParcelaService{
	
	@Autowired
	private ParcelaRepository parcelaRepository;
	
	@Autowired
	private ClienteRepository clienteRepository;

	@Override
	@Transactional(readOnly = true)
	public List<ParcelaDto> getAllByCliente(Integer idCliente) {
		Cliente cliente = clienteRepository.findById(idCliente).orElse(null);
		List<Parcela> parcelas = parcelaRepository.findByCliente(cliente);
		ParcelaDto parcelaDto = new ParcelaDto();
		List<ParcelaDto> parcelasDto = new ArrayList<ParcelaDto>();
		ParcelaMapper mapper = Mappers.getMapper(ParcelaMapper.class);
		for(int i = 0; i < parcelas.size(); i++) {
			parcelaDto = mapper.convertParcelaToParcelaDto(parcelas.get(i));
			parcelasDto.add(parcelaDto);
		}
		return parcelasDto;
	}

	@Override
	@Transactional
	public Integer addParcela(ParcelaDto parcelaDto) {
		ParcelaMapper mapper = Mappers.getMapper(ParcelaMapper.class);
		Parcela parcela = mapper.convertParcelaDtoToParcela(parcelaDto);
		return parcelaRepository.save(parcela).getIdParcela();
	}

	@Override
	@Transactional
	public void removeParcela(Integer idParcela) {
		parcelaRepository.deleteParcela(idParcela);
		
	}

	@Override
	@Transactional(readOnly = true)
	public ParcelaDto getById(Integer idParcela) {
		ParcelaMapper mapper = Mappers.getMapper(ParcelaMapper.class);
		Parcela parcela = parcelaRepository.findById(idParcela).orElse(null);
		ParcelaDto parcelaDto = mapper.convertParcelaToParcelaDto(parcela);
		return parcelaDto;
	}

	@Override
	@Transactional(readOnly = true)
	public List<ParcelaDto> getAll() {
		List<Parcela> parcelas = parcelaRepository.findAll();
		ParcelaDto parcelaDto = new ParcelaDto();
		List<ParcelaDto> parcelasDto = new ArrayList<ParcelaDto>();
		ParcelaMapper mapper = Mappers.getMapper(ParcelaMapper.class);
		for(int i = 0; i < parcelas.size(); i++) {
			parcelaDto = mapper.convertParcelaToParcelaDto(parcelas.get(i));
			parcelasDto.add(parcelaDto);
		}
		return parcelasDto;
	}

}
