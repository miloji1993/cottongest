package com.agroloji.cottongest.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.agroloji.cottongest.dto.ClienteDto;
import com.agroloji.cottongest.entity.Cliente;
import com.agroloji.cottongest.mapper.ClienteMapper;
import com.agroloji.cottongest.repository.ClienteRepository;
import com.agroloji.cottongest.service.ClienteService;


@Service
public class ClienteServiceImpl implements ClienteService{
	
	@Autowired
	private ClienteRepository clienteRepository;

	@Override
	@Transactional(readOnly = true)
	public List<ClienteDto> getAll() {
		List<Cliente> clientes = clienteRepository.findByActivo(true);
		ClienteDto clienteDto = new ClienteDto();
		List<ClienteDto> clientesDto = new ArrayList<ClienteDto>();
		ClienteMapper mapper = Mappers.getMapper(ClienteMapper.class);
		for(int i = 0; i < clientes.size(); i++) {
			clienteDto = mapper.convertClienteToClienteDto(clientes.get(i));
			clientesDto.add(clienteDto);
		}
		return clientesDto;
	}

	@Override
	@Transactional(readOnly = true)
	public ClienteDto getById(Integer id) {
		ClienteMapper mapper = Mappers.getMapper(ClienteMapper.class);
		Cliente cliente = clienteRepository.findById(id).orElse(null);
		ClienteDto clienteDto = mapper.convertClienteToClienteDto(cliente);
		return clienteDto;
	}

	@Override
	@Transactional
	public Integer addCliente(ClienteDto clienteDto) {
		ClienteMapper mapper = Mappers.getMapper(ClienteMapper.class);
		Cliente cliente = mapper.convertClienteDtoToCliente(clienteDto);
		return clienteRepository.save(cliente).getIdCliente();
	}

	@Override
	@Transactional
	public void removeCliente(Integer id) {
		clienteRepository.deleteCliente(false, id);
		
	}

}
