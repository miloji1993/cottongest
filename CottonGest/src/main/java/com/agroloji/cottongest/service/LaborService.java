package com.agroloji.cottongest.service;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.agroloji.cottongest.dto.LaborDto;
import com.agroloji.cottongest.entity.Parcela;

public interface LaborService {

	public List<LaborDto> getAllByFecha();
	public List<LaborDto> getFilterByFechaOrParcela(Date fecha, Parcela parcela);
	public LaborDto 	  getById(Integer id);
	public Integer		  addLabor(LaborDto laborDto);
	public void 		  removeLabor(Integer id);
	public Page<LaborDto> getAllByFecha(Pageable pageable);
	public Page<LaborDto> getFilterByFechaOrParcela(Date fecha, Parcela parcela, Pageable pageable);
}
