package com.agroloji.cottongest.service;

import java.util.List;

import com.agroloji.cottongest.dto.ParcelaDto;

public interface ParcelaService {
	
	public List<ParcelaDto> getAllByCliente(Integer idCliente);
	public List<ParcelaDto> getAll();
	public ParcelaDto		getById(Integer idParcela);
	public Integer			addParcela(ParcelaDto parcela);
	public void				removeParcela(Integer idParcela);

}
