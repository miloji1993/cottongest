package com.agroloji.cottongest.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.agroloji.cottongest.dto.TrabajadorDto;
import com.agroloji.cottongest.entity.Trabajador;
import com.agroloji.cottongest.mapper.TrabajadorMapper;
import com.agroloji.cottongest.repository.TrabajadorRepository;
import com.agroloji.cottongest.service.TrabajadorService;

@Service
public class TrabajadorServiceImpl implements TrabajadorService{
	
	@Autowired
	private TrabajadorRepository trabajadorRepository;

	@Override
	@Transactional(readOnly = true)
	public List<TrabajadorDto> getAll() {
		
		List<Trabajador> trabajadores = trabajadorRepository.findByActivo(true);
		TrabajadorDto trabajadorDto = new TrabajadorDto();
		List<TrabajadorDto> trabajadoresDto = new ArrayList<TrabajadorDto>();
		TrabajadorMapper mapper = Mappers.getMapper(TrabajadorMapper.class);
		for(int i = 0; i < trabajadores.size(); i++) {
			trabajadorDto = mapper.convertTrabajadorToTrabajadorDto(trabajadores.get(i));
			trabajadoresDto.add(trabajadorDto);
		}
		return trabajadoresDto;
		
	}

	@Override
	@Transactional(readOnly = true)
	public TrabajadorDto getById(Integer id) {
		TrabajadorMapper mapper = Mappers.getMapper(TrabajadorMapper.class);
		Trabajador trabajador = trabajadorRepository.findById(id).orElse(null);
		TrabajadorDto trabajadorDto = mapper.convertTrabajadorToTrabajadorDto(trabajador);
		return trabajadorDto;
	}

	@Override
	@Transactional
	public Integer addTrabajador(TrabajadorDto trabajadorDto) {
		TrabajadorMapper mapper = Mappers.getMapper(TrabajadorMapper.class);
		Trabajador trabajador = mapper.convertTrabajadorDtoToTrabajador(trabajadorDto);
		return trabajadorRepository.save(trabajador).getIdTrabajador();
	}

	@Override
	@Transactional
	public void removeTrabajador(Integer id) {
		trabajadorRepository.deleteTrabajador(false, id);
		
	}

}
