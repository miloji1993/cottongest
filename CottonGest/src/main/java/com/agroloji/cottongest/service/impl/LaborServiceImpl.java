package com.agroloji.cottongest.service.impl;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.agroloji.cottongest.dto.LaborDto;
import com.agroloji.cottongest.entity.Labor;
import com.agroloji.cottongest.entity.Parcela;
import com.agroloji.cottongest.mapper.LaborMapper;
import com.agroloji.cottongest.repository.LaborRepository;
import com.agroloji.cottongest.service.LaborService;

@Service
public class LaborServiceImpl implements LaborService{
	
	@Autowired
	private LaborRepository laborRepository;

	@Override
	@Transactional(readOnly = true)
	public List<LaborDto> getAllByFecha() {
		Date date = new Date();
		List<Labor> labores = laborRepository.findByFecha(date);
		LaborDto laborDto = new LaborDto();
		List<LaborDto> laboresDto = new ArrayList<LaborDto>();
		LaborMapper mapper = Mappers.getMapper(LaborMapper.class);
		for(int i = 0; i < labores.size(); i++) {
			laborDto = mapper.convertLaborToLaborDto(labores.get(i));
			laboresDto.add(laborDto);
		}
		return laboresDto;
	}

	@Override
	@Transactional
	public Integer addLabor(LaborDto laborDto) {
		Double entrada = (double) (laborDto.getEntrada().getTime() / 3600000);
		Double salida = (double) (laborDto.getSalida().getTime() / 3600000);
		System.out.println(entrada);
		System.out.println(salida);
		laborDto.setHoras(salida - entrada);
		System.out.println(laborDto.getHoras());
		LaborMapper mapper = Mappers.getMapper(LaborMapper.class);
		Labor labor = mapper.convertLaborDtoToLabor(laborDto);
		return laborRepository.save(labor).getIdLabor();
	}

	@Override
	@Transactional
	public void removeLabor(Integer id) {
		laborRepository.deleteById(id);
		
	}

	@Override
	@Transactional(readOnly = true)
	public LaborDto getById(Integer id) {
		LaborMapper mapper = Mappers.getMapper(LaborMapper.class);
		Labor labor = laborRepository.findById(id).orElse(null);
		LaborDto laborDto = mapper.convertLaborToLaborDto(labor);
		return laborDto;
	}

	@Override
	@Transactional(readOnly = true)
	public List<LaborDto> getFilterByFechaOrParcela(Date fecha, Parcela parcela) {
		List<Labor> labores = laborRepository.findByFechaOrParcela(fecha, parcela);
		LaborDto laborDto = new LaborDto();
		List<LaborDto> laboresDto = new ArrayList<LaborDto>();
		LaborMapper mapper = Mappers.getMapper(LaborMapper.class);
		for(int i = 0; i < labores.size(); i++) {
			laborDto = mapper.convertLaborToLaborDto(labores.get(i));
			laboresDto.add(laborDto);
		}
		return laboresDto;
	}

	@Override
	@Transactional(readOnly = true)
	public Page<LaborDto> getAllByFecha(Pageable pageable) {
		int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;
        List<Labor> labores = laborRepository.findByFecha(new Date());
		LaborDto laborDto = new LaborDto();
		List<LaborDto> laboresDto = new ArrayList<LaborDto>();
		LaborMapper mapper = Mappers.getMapper(LaborMapper.class);
		for(int i = 0; i < labores.size(); i++) {
			laborDto = mapper.convertLaborToLaborDto(labores.get(i));
			laboresDto.add(laborDto);
		}
		List<LaborDto> laboresDosDto;
		if(laboresDto.size() < startItem) {
			laboresDosDto = Collections.emptyList();
		}else {
			int toIndex = Math.min(startItem + pageSize, laboresDto.size());
			laboresDosDto = laboresDto.subList(startItem, toIndex);
		}
		
		Page<LaborDto> laborPage = new PageImpl<LaborDto>(laboresDosDto, PageRequest.of(currentPage, pageSize), laboresDto.size());
		return laborPage;
	}

	@Override
	public Page<LaborDto> getFilterByFechaOrParcela(Date fecha, Parcela parcela, Pageable pageable) {
		int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;
        List<Labor> labores = laborRepository.findByFechaOrParcela(fecha, parcela);
		LaborDto laborDto = new LaborDto();
		List<LaborDto> laboresDto = new ArrayList<LaborDto>();
		LaborMapper mapper = Mappers.getMapper(LaborMapper.class);
		for(int i = 0; i < labores.size(); i++) {
			laborDto = mapper.convertLaborToLaborDto(labores.get(i));
			laboresDto.add(laborDto);
		}
		List<LaborDto> laboresDosDto;
		if(laboresDto.size() < startItem) {
			laboresDosDto = Collections.emptyList();
		}else {
			int toIndex = Math.min(startItem + pageSize, laboresDto.size());
			laboresDosDto = laboresDto.subList(startItem, toIndex);
		}
		
		Page<LaborDto> laborPage = new PageImpl<LaborDto>(laboresDosDto, PageRequest.of(currentPage, pageSize), laboresDto.size());
		return laborPage;
	}
	
	
	

}
