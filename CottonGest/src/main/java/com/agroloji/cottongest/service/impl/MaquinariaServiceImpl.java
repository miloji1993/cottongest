package com.agroloji.cottongest.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.agroloji.cottongest.dto.MaquinariaDto;
import com.agroloji.cottongest.entity.Maquinaria;
import com.agroloji.cottongest.mapper.MaquinariaMapper;
import com.agroloji.cottongest.repository.MaquinariaRepository;
import com.agroloji.cottongest.service.MaquinariaService;

@Service
public class MaquinariaServiceImpl implements MaquinariaService{
	
	@Autowired
	private MaquinariaRepository maquinariaRepository;

	@Override
	@Transactional(readOnly = true)
	public List<MaquinariaDto> getAll() {
		List<Maquinaria> maquinas = maquinariaRepository.findAll();
		MaquinariaDto maquinariaDto = new MaquinariaDto();
		List<MaquinariaDto> maquinasDto = new ArrayList<MaquinariaDto>();
		MaquinariaMapper mapper = Mappers.getMapper(MaquinariaMapper.class);
		for(int i = 0; i < maquinas.size(); i++) {
			maquinariaDto = mapper.convertMaquinariaToMaquinariaDto(maquinas.get(i));
			maquinasDto.add(maquinariaDto);
			
		}
		return maquinasDto;
	}

	@Override
	@Transactional(readOnly = true)
	public MaquinariaDto getById(Integer id) {
		MaquinariaMapper mapper = Mappers.getMapper(MaquinariaMapper.class);
		Maquinaria maquinaria = maquinariaRepository.findById(id).orElse(null);
		MaquinariaDto maquinariaDto = mapper.convertMaquinariaToMaquinariaDto(maquinaria);
		return maquinariaDto;
	}

	@Override
	@Transactional
	public Integer addMaquinaria(MaquinariaDto maquinariaDto) {
		MaquinariaMapper mapper = Mappers.getMapper(MaquinariaMapper.class);
		Maquinaria maquinaria = mapper.convertMaquinariaDtoToMaquinaria(maquinariaDto);
		return maquinariaRepository.save(maquinaria).getIdMaquinaria();
	}

	@Override
	@Transactional
	public void removeMaquinaria(Integer id) {
		maquinariaRepository.deleteById(id);
		
	}

}
