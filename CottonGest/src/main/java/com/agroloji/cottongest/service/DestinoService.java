package com.agroloji.cottongest.service;

import java.util.List;

import com.agroloji.cottongest.dto.DestinoDto;

public interface DestinoService {
	
	public List<DestinoDto> getAll();
	public DestinoDto 		getById(Integer id);
	public Integer			addDestino(DestinoDto destinoDto);
	public void				removeDestino(Integer id);

}
