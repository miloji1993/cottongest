package com.agroloji.cottongest.service;

import java.util.List;

import com.agroloji.cottongest.dto.ClienteDto;

public interface ClienteService {
	
	public List<ClienteDto> getAll();
	public ClienteDto 		getById(Integer id);
	public Integer			addCliente(ClienteDto clienteDto);
	public void				removeCliente(Integer id);

}
