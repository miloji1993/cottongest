package com.agroloji.cottongest.service;

import java.util.List;

import com.agroloji.cottongest.dto.TrabajadorDto;

public interface TrabajadorService {
	
	public List<TrabajadorDto> getAll();
	public TrabajadorDto	   getById(Integer id);
	public Integer			   addTrabajador(TrabajadorDto trabajadorDto);
	public void				   removeTrabajador(Integer id);

}
