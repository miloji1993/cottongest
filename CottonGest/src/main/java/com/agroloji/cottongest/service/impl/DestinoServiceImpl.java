package com.agroloji.cottongest.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.agroloji.cottongest.dto.DestinoDto;
import com.agroloji.cottongest.entity.Destino;
import com.agroloji.cottongest.mapper.DestinoMapper;
import com.agroloji.cottongest.repository.DestinoRepository;
import com.agroloji.cottongest.service.DestinoService;

@Service
public class DestinoServiceImpl implements DestinoService {
	
	@Autowired
	private DestinoRepository destinoRepository;

	@Override
	@Transactional(readOnly = true)
	public List<DestinoDto> getAll() {
		List<Destino> destinos = destinoRepository.findAll();
		DestinoDto destinoDto = new DestinoDto();
		List<DestinoDto> destinosDto = new ArrayList<DestinoDto>();
		DestinoMapper mapper = Mappers.getMapper(DestinoMapper.class);
		for(int i = 0; i < destinos.size(); i++) {
			destinoDto = mapper.convertDestinoToDestinoDto(destinos.get(i));
			destinosDto.add(destinoDto);
		}
		return destinosDto;
	}

	@Override
	@Transactional(readOnly = true)
	public DestinoDto getById(Integer id) {
		Destino destino = destinoRepository.findById(id).orElse(null);
		DestinoMapper mapper = Mappers.getMapper(DestinoMapper.class);
		DestinoDto destinoDto = mapper.convertDestinoToDestinoDto(destino);
		return destinoDto;
	}

	@Override
	@Transactional
	public Integer addDestino(DestinoDto destinoDto) {
		DestinoMapper mapper = Mappers.getMapper(DestinoMapper.class);
		Destino destino = mapper.convertDestinoDtoToDestino(destinoDto);
		return destinoRepository.save(destino).getIdDestino();
	}

	@Override
	@Transactional
	public void removeDestino(Integer id) {
		destinoRepository.deleteById(id);
		
	}

}
