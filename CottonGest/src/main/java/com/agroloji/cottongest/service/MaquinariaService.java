package com.agroloji.cottongest.service;

import java.util.List;

import com.agroloji.cottongest.dto.MaquinariaDto;

public interface MaquinariaService {
	
	public List<MaquinariaDto> getAll();
	public MaquinariaDto	   getById(Integer id);
	public Integer			   addMaquinaria(MaquinariaDto maquinariaDto);
	public void				   removeMaquinaria(Integer id);

}
