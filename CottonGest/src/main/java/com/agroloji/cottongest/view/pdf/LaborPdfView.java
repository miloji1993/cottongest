package com.agroloji.cottongest.view.pdf;

import java.awt.Font;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.document.AbstractPdfView;

import com.agroloji.cottongest.dto.LaborDto;
import com.lowagie.text.Document;
import com.lowagie.text.PageSize;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

@Component("home")
public class LaborPdfView extends AbstractPdfView{

	@Override
	protected Document newDocument() {
	  return new Document(PageSize.A4.rotate());
	}
	
	@Override
	protected void buildPdfDocument(Map<String, Object> model, Document document, PdfWriter writer,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		Page<LaborDto> labores = (Page<LaborDto>) model.get("labores");
		Phrase frase = new Phrase("DATOS CAMPAÑA ALGODON");
		PdfPTable tabla = new PdfPTable(11);
		tabla.setWidthPercentage(105);
		tabla.setSpacingAfter(20);
		PdfPCell cell1 = new PdfPCell(new Phrase("Ref"));
		cell1.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
		tabla.addCell(cell1);
		PdfPCell cell2 = new PdfPCell(new Phrase("Parcela"));
		cell2.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
		tabla.addCell(cell2);
		PdfPCell cell3 = new PdfPCell(new Phrase("Cliente"));
		cell3.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
		tabla.addCell(cell3);
		PdfPCell cell4 = new PdfPCell(new Phrase("Fecha"));
		cell4.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
		tabla.addCell(cell4);
		PdfPCell cell5 = new PdfPCell(new Phrase("Trabajador"));
		cell5.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
		tabla.addCell(cell5);
		PdfPCell cell6 = new PdfPCell(new Phrase("Hora Entrada"));
		cell6.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
		tabla.addCell(cell6);
		PdfPCell cell7 = new PdfPCell(new Phrase("Hora Salida"));
		cell7.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
		tabla.addCell(cell7);	
		PdfPCell cell8 = new PdfPCell(new Phrase("Horas Totales"));
		cell8.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
		tabla.addCell(cell8);
		PdfPCell cell9 = new PdfPCell(new Phrase("Maquinaria"));
		cell9.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
		cell9.setVerticalAlignment(PdfPCell.ALIGN_CENTER);
		tabla.addCell(cell9);
		PdfPCell cell10 = new PdfPCell(new Phrase("Kilos"));
		cell10.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
		tabla.addCell(cell10);
		PdfPCell cell11 = new PdfPCell(new Phrase("Descripcion"));
		cell11.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
		tabla.addCell(cell11);
		
		
		for(LaborDto labor: labores) {
			tabla.addCell(labor.getIdLabor().toString());
			tabla.addCell(labor.getParcela().getNombre());
			tabla.addCell(labor.getParcela().getCliente().getAlias());
			tabla.addCell(labor.getFecha().toString());
			tabla.addCell(labor.getEntrada().toString());
			tabla.addCell(labor.getSalida().toString());
			tabla.addCell(labor.getTrabajador().getNombre()+' '+labor.getTrabajador().getApellidos());
			tabla.addCell(String.valueOf(labor.getHoras()));
			tabla.addCell(labor.getMaquinaria().getNombre());
			tabla.addCell(String.valueOf(labor.getKilos()));
			tabla.addCell(labor.getDescripcion());
		}
		tabla.setHorizontalAlignment(tabla.ALIGN_CENTER);
		document.add(frase);
		document.add(tabla);
		
		

		
		
	}

}
