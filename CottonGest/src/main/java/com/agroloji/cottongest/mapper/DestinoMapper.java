package com.agroloji.cottongest.mapper;

import org.mapstruct.Mapper;

import com.agroloji.cottongest.dto.DestinoDto;
import com.agroloji.cottongest.entity.Destino;

@Mapper
public interface DestinoMapper {
	
	DestinoDto convertDestinoToDestinoDto(Destino destino);
	Destino    convertDestinoDtoToDestino(DestinoDto destinoDto);

}
