package com.agroloji.cottongest.mapper;

import org.mapstruct.Mapper;

import com.agroloji.cottongest.dto.ParcelaDto;
import com.agroloji.cottongest.entity.Parcela;

@Mapper
public interface ParcelaMapper {
	
	ParcelaDto convertParcelaToParcelaDto(Parcela parcela);
	Parcela	   convertParcelaDtoToParcela(ParcelaDto parcelaDto);

}
