package com.agroloji.cottongest.mapper;

import org.mapstruct.Mapper;

import com.agroloji.cottongest.dto.LaborDto;
import com.agroloji.cottongest.entity.Labor;

@Mapper
public interface LaborMapper {
	
	LaborDto convertLaborToLaborDto(Labor labor);
	Labor 	 convertLaborDtoToLabor(LaborDto laborDto);

}
