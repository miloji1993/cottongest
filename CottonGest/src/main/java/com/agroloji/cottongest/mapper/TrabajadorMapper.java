package com.agroloji.cottongest.mapper;

import org.mapstruct.Mapper;

import com.agroloji.cottongest.dto.TrabajadorDto;
import com.agroloji.cottongest.entity.Trabajador;

@Mapper
public interface TrabajadorMapper {
	
	TrabajadorDto convertTrabajadorToTrabajadorDto(Trabajador trabajador);
	Trabajador 	  convertTrabajadorDtoToTrabajador(TrabajadorDto trabajadorDto);

}
