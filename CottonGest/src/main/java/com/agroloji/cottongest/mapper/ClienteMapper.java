package com.agroloji.cottongest.mapper;

import org.mapstruct.Mapper;

import com.agroloji.cottongest.dto.ClienteDto;
import com.agroloji.cottongest.entity.Cliente;

@Mapper
public interface ClienteMapper {

	ClienteDto convertClienteToClienteDto(Cliente cliente);
	Cliente    convertClienteDtoToCliente(ClienteDto clienteDto);
}
