package com.agroloji.cottongest.mapper;

import org.mapstruct.Mapper;

import com.agroloji.cottongest.dto.MaquinariaDto;
import com.agroloji.cottongest.entity.Maquinaria;

@Mapper
public interface MaquinariaMapper {
	
	MaquinariaDto convertMaquinariaToMaquinariaDto(Maquinaria maquinaria);
	Maquinaria 	  convertMaquinariaDtoToMaquinaria(MaquinariaDto maquinariaDto);

}
