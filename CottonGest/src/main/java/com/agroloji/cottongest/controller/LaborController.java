package com.agroloji.cottongest.controller;

import java.time.LocalDate;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.agroloji.cottongest.dto.DestinoDto;
import com.agroloji.cottongest.dto.LaborDto;
import com.agroloji.cottongest.dto.MaquinariaDto;
import com.agroloji.cottongest.dto.ParcelaDto;
import com.agroloji.cottongest.dto.TrabajadorDto;
import com.agroloji.cottongest.filter.LaborFiltro;
import com.agroloji.cottongest.paginator.PageRender;
import com.agroloji.cottongest.service.DestinoService;
import com.agroloji.cottongest.service.LaborService;
import com.agroloji.cottongest.service.MaquinariaService;
import com.agroloji.cottongest.service.ParcelaService;
import com.agroloji.cottongest.service.TrabajadorService;

@Controller
@RequestMapping(value = {"/labor", "/"})
public class LaborController {
	
	@Autowired
	private LaborService laborService;
	
	@Autowired
	private MaquinariaService maquinariaService;
	
	@Autowired
	private TrabajadorService trabajadorService;
	
	@Autowired
	private DestinoService destinoService;
	
	@Autowired
	private ParcelaService parcelaService;
	
	
	
	@GetMapping("/")
	public ModelAndView home(@RequestParam(name="page", defaultValue="0") int page, HttpSession session){
		ModelAndView model = new ModelAndView();
		Pageable pageRequest = PageRequest.of(page, 3);
		LaborFiltro filtro = (LaborFiltro) session.getAttribute("filtro");
		Page<LaborDto> labores;
		if(filtro == null) {
			labores = laborService.getAllByFecha(pageRequest);
		}else {
			labores = laborService.getFilterByFechaOrParcela(filtro.getFecha(), filtro.getParcela(), pageRequest);
		}	
		PageRender<LaborDto> pageRender = new PageRender<>("/", labores);
		List<ParcelaDto> parcelas = parcelaService.getAll();
		model.addObject("labores", labores);
		model.addObject("parcelas", parcelas);
		model.addObject("filtroFecha", new LaborFiltro());
		model.addObject("localDate", LocalDate.now());
		model.addObject("page", pageRender);
		model.setViewName("home");
		return model;
	}
	
	@GetMapping("/details/{id}")
	public ModelAndView detalles(@PathVariable("id") Integer idLabor, RedirectAttributes mensaje) {
		ModelAndView model = new ModelAndView();
		LaborDto laborDto = laborService.getById(idLabor);
		if(laborDto == null) {
			mensaje.addFlashAttribute("error", "Fallo al encontrar la labor");
			model.setViewName("redirect:/");
		}else {
			model.addObject("labor", laborDto);
			model.setViewName("/detalle");
		}
		return model;
	}
	
	@GetMapping("/insertar")
	public ModelAndView insertar() {
		ModelAndView model = new ModelAndView();
		LaborDto labor = new LaborDto();
		List<MaquinariaDto> maquinas = maquinariaService.getAll();
		List<TrabajadorDto> trabajadores = trabajadorService.getAll();
		List<DestinoDto> destinos = destinoService.getAll();
		List<ParcelaDto> parcelas = parcelaService.getAll();
		model.addObject("labor", labor);
		model.addObject("maquinas", maquinas);
		model.addObject("trabajadores", trabajadores);
		model.addObject("destinos", destinos);
		model.addObject("parcelas", parcelas);
		model.addObject("titulo", "Insertar Labor");
		model.setViewName("/insertar");
		return model;
		
	}
	
	@GetMapping("/editar/{id}")
	public ModelAndView editar(@PathVariable("id") Integer idLabor, RedirectAttributes mensaje) {
		ModelAndView model = new ModelAndView();
		LaborDto labor = laborService.getById(idLabor);
		List<MaquinariaDto> maquinas = maquinariaService.getAll();
		List<TrabajadorDto> trabajadores = trabajadorService.getAll();
		List<DestinoDto> destinos = destinoService.getAll();
		List<ParcelaDto> parcelas = parcelaService.getAll();
		if(labor != null) {
			model.addObject("labor", labor);
			model.addObject("maquinas", maquinas);
			model.addObject("trabajadores", trabajadores);
			model.addObject("destinos", destinos);
			model.addObject("parcelas", parcelas);
			model.addObject("titulo", "Editar Labor");
			model.setViewName("/insertar");
		}else {
			mensaje.addFlashAttribute("error", "Fallo al encontrar la labor");
			model.setViewName("redirect:/");
		}
		return model;
	}
	
	@PostMapping("/guardar")
	public ModelAndView guardar(@ModelAttribute() LaborDto labor, RedirectAttributes mensaje) {
		ModelAndView model = new ModelAndView("redirect:/");
		Integer count = laborService.addLabor(labor);
		if(count != 0) {
			mensaje.addFlashAttribute("exito", "Labor guardada con exito");
		}else {
			mensaje.addFlashAttribute("error", "Fallo al guardar labor");
		}
		return model;
	}
	
	@GetMapping("/eliminar/{id}")
	public ModelAndView eliminar(@PathVariable("id") Integer idLabor, RedirectAttributes mensaje) {
		ModelAndView model = new ModelAndView("redirect:/");
		LaborDto laborDto = laborService.getById(idLabor);
		if(laborDto != null) {
			laborService.removeLabor(idLabor);
			mensaje.addFlashAttribute("exito", "Destino eliminado con exito");
		}else {
			mensaje.addFlashAttribute("error", "Fallo al encontrar destino");
		}
		return model;	
	}
	
	@PostMapping("/filtrofecha")
	public ModelAndView filtrar(@ModelAttribute("filtroFecha") LaborFiltro filtro, @RequestParam(name="page", defaultValue="0") int page, HttpSession session) {
		ModelAndView model = new ModelAndView();
		Pageable pageRequest = PageRequest.of(page, 3);
		Page<LaborDto> labores = laborService.getFilterByFechaOrParcela(filtro.getFecha(), filtro.getParcela(), pageRequest);
		PageRender<LaborDto> pageRender = new PageRender<>("/", labores);
		session.setAttribute("filtro", filtro);
		List<ParcelaDto> parcelas = parcelaService.getAll();
		model.addObject("labores", labores);
		model.addObject("parcelas",parcelas);
		model.addObject("filtroFecha", new LaborFiltro());
		model.addObject("localDate", LocalDate.now());
		model.addObject("page", pageRender);
		model.setViewName("home");
		return model;
	}

}
