package com.agroloji.cottongest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.agroloji.cottongest.dto.DestinoDto;
import com.agroloji.cottongest.service.DestinoService;

@Controller
@RequestMapping("/destino")
public class DestinoController {
	
	@Autowired
	private DestinoService destinoService;
	
	@GetMapping("/lista")
	public ModelAndView listado() {
		ModelAndView model = new ModelAndView();
		List<DestinoDto> destinos = destinoService.getAll();
		model.addObject("destinos", destinos);
		model.setViewName("/destino/lista");
		return model;
	}
	
	@GetMapping("/details/{id}")
	public ModelAndView detalles(@PathVariable("id") Integer idDestino, RedirectAttributes mensaje) {
		ModelAndView model = new ModelAndView();
		DestinoDto destinoDto = destinoService.getById(idDestino);
		if(destinoDto != null) {
			model.addObject("destino", destinoDto);
			model.setViewName("/destino/detalle");
		}else {
			mensaje.addFlashAttribute("error", "Fallo al encontrar destino");
			model.setViewName("redirect:/destino/lista");
		}
		return model;
	}
	
	@GetMapping("/insertar")
	public ModelAndView insertar() {
		DestinoDto destinoDto = new DestinoDto();
		ModelAndView model = new ModelAndView();
		model.addObject("destino", destinoDto);
		model.addObject("titulo", "Insertar Destino");
		model.setViewName("/destino/insertar");
		return model;
	}
	
	@GetMapping("/editar/{id}")
	public ModelAndView editar(@PathVariable("id") Integer idDestino, RedirectAttributes mensaje) {
		ModelAndView model = new ModelAndView();
		DestinoDto destino = destinoService.getById(idDestino);
		if(destino != null) {
			model.addObject("destino", destino);
			model.addObject("titulo", "Editar Destino");
			model.setViewName("/destino/insertar");
		}else {
			mensaje.addFlashAttribute("error", "Fallo al encontrar destino");
			model.setViewName("redirect:/destino/lista");
		}
		return model;
	}
	
	@GetMapping("/eliminar/{id}")
	public ModelAndView eliminar(@PathVariable("id") Integer idDestino, RedirectAttributes mensaje) {
		ModelAndView model = new ModelAndView("redirect:/destino/lista");
		DestinoDto destinoDto = destinoService.getById(idDestino);
		if(destinoDto != null) {
			destinoService.removeDestino(idDestino);
			mensaje.addFlashAttribute("exito", "Destino eliminado con exito");
		}else {
			mensaje.addFlashAttribute("error", "Fallo al encontrar destino");
		}
		return model;	
	}
	
	@PostMapping("/guardar")
	public ModelAndView guardar(@ModelAttribute() DestinoDto destino, RedirectAttributes mensaje) {
		ModelAndView model = new ModelAndView("redirect:/destino/lista");
		destinoService.addDestino(destino);
		mensaje.addFlashAttribute("exito", "Destino guardado con exito");
		return model;
	}
	

}
