package com.agroloji.cottongest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.agroloji.cottongest.dto.ClienteDto;
import com.agroloji.cottongest.service.ClienteService;


@Controller
@RequestMapping("/cliente")
public class ClienteController {
	
	@Autowired
	private ClienteService clienteService;
	
	@GetMapping("/lista")
	public ModelAndView listado() {
		ModelAndView model = new ModelAndView();
		List<ClienteDto> clientes = clienteService.getAll();
		model.addObject("clientes", clientes);
		model.setViewName("cliente/lista");
		return model;
	}
	
	@GetMapping("/details/{id}")
	public ModelAndView detalles(@PathVariable("id") Integer idCliente, RedirectAttributes mensaje) {
		ModelAndView model = new ModelAndView();
		ClienteDto clienteDto = clienteService.getById(idCliente);
		if(clienteDto == null) {
			mensaje.addFlashAttribute("error", "Fallo al encontrar Cliente");
			model.setViewName("redirect:/cliente/lista");
		}else {
			model.addObject("cliente", clienteDto);
			model.setViewName("cliente/detalle");
		}
		return model;
	}
	
	@GetMapping("/insertar")
	public ModelAndView insertar() {
		ClienteDto clienteDto = new ClienteDto();
		ModelAndView model = new ModelAndView();
		model.addObject("cliente", clienteDto);
		model.addObject("titulo", "Insertar Cliente");
		model.setViewName("cliente/insertar");
		return model;
	}
	
	@GetMapping("/editar/{id}")
	public ModelAndView editar(@PathVariable("id") Integer idCliente, RedirectAttributes mensaje) {
		ModelAndView model = new ModelAndView();
		ClienteDto cliente = clienteService.getById(idCliente);
		if(cliente != null) {
			model.addObject("cliente", cliente);
			model.addObject("titulo", "Editar Cliente");
			model.setViewName("cliente/insertar");
		}else {
			mensaje.addFlashAttribute("error", "Fallo al encontrar Cliente");
			model.setViewName("redirect:/cliente/lista");
		}
		return model;
	}
	
	@GetMapping("/eliminar/{id}")
	public ModelAndView  eliminar(@PathVariable("id") Integer idCliente, RedirectAttributes mensaje) {
		ModelAndView model = new ModelAndView("redirect:/cliente/lista");
		ClienteDto cliente = clienteService.getById(idCliente);
		if(cliente != null) {
			clienteService.removeCliente(idCliente);
			mensaje.addFlashAttribute("exito", "Cliente eliminado con exito");
		}else {
			mensaje.addFlashAttribute("error", "Fallo al eliminar Cliente");
		}
		return model;
	}
	
	@PostMapping("/guardar")
	public ModelAndView guardar(@ModelAttribute() ClienteDto cliente, RedirectAttributes mensaje) {
		ModelAndView model = new ModelAndView("redirect:/cliente/lista");
		cliente.setActivo(true);
		clienteService.addCliente(cliente);
		mensaje.addFlashAttribute("exito", "Cliente guardado con exito");
		return model;
	}

}
