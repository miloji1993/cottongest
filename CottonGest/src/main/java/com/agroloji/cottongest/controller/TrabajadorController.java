package com.agroloji.cottongest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.agroloji.cottongest.dto.TrabajadorDto;
import com.agroloji.cottongest.service.TrabajadorService;

@Controller
@RequestMapping("/trabajador")
public class TrabajadorController {
	
	@Autowired
	private TrabajadorService trabajadorService;
	
	@GetMapping("/lista")
	public ModelAndView listado() {
		ModelAndView model = new ModelAndView();
		List<TrabajadorDto> trabajadores = trabajadorService.getAll();
		model.addObject("trabajadores", trabajadores);
		model.setViewName("trabajador/listatrabajadores");
		return model;
	}
	
	@GetMapping("/details/{id}")
	public ModelAndView detalles(@PathVariable("id") Integer idTrabajador, RedirectAttributes mensaje) {
		ModelAndView model = new ModelAndView();
		TrabajadorDto trabajadorDto = trabajadorService.getById(idTrabajador);
		if(trabajadorDto == null) {
			mensaje.addFlashAttribute("error", "Fallo al encontrar Trabajador");
			model.setViewName("redirect:/trabajador/listatrabajadores");
		}else {
			model.addObject("trabajador", trabajadorDto);
			model.setViewName("trabajador/detalle");
		}
		return model;
	}
	
	@GetMapping("/insertar")
	public ModelAndView insertar() {
		TrabajadorDto trabajadorDto = new TrabajadorDto();
		ModelAndView model = new ModelAndView();
		model.addObject("trabajador", trabajadorDto);
		model.addObject("titulo", "Insertar Trabajador");
		model.setViewName("trabajador/insertar");
		return model;
	}
	
	@GetMapping("/editar/{id}")
	public ModelAndView editar(@PathVariable("id") Integer idTrabajador, RedirectAttributes mensaje) {
		ModelAndView model = new ModelAndView();
		TrabajadorDto trabajador = trabajadorService.getById(idTrabajador);
		if(trabajador != null) {
			model.addObject("trabajador", trabajador);
			model.addObject("titulo", "Editar Trabajador");
			model.setViewName("trabajador/insertar");
		}else {
			mensaje.addFlashAttribute("error", "Fallo al encontrar Trabajador");
			model.setViewName("redirect:/trabajador/listatrabajadores");
		}
		return model;
	}
	
	@GetMapping("/eliminar/{id}")
	public ModelAndView  eliminar(@PathVariable("id") Integer idTrabajador, RedirectAttributes mensaje) {
		ModelAndView model = new ModelAndView("redirect:/trabajador/listatrabajadores");
		TrabajadorDto trabajador = trabajadorService.getById(idTrabajador);
		if(trabajador != null) {
			trabajadorService.removeTrabajador(idTrabajador);
			mensaje.addFlashAttribute("exito", "Trabajador eliminado con exito");
		}else {
			mensaje.addFlashAttribute("error", "Fallo al eliminar trabajador");
		}
		return model;
	}
	
	@PostMapping("/guardar")
	public ModelAndView guardar(@ModelAttribute() TrabajadorDto trabajador, RedirectAttributes mensaje) {
		ModelAndView model = new ModelAndView("redirect:/trabajador/listatrabajadores");
		trabajador.setActivo(true);
		trabajadorService.addTrabajador(trabajador);
		mensaje.addFlashAttribute("exito", "Trabajador guardado con exito");
		return model;
	}

}
