package com.agroloji.cottongest.controller;

import java.security.Principal;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class LoginController {
	
	@GetMapping("/login")
	public ModelAndView login(@RequestParam(value = "error", required=false) String error, @RequestParam(value = "logout", required=false) String logout, Principal principal, RedirectAttributes mensaje) {
		ModelAndView model = new ModelAndView();
		if(principal != null) {
			mensaje.addFlashAttribute("info", "Ya Iniciaste Sesión Anteriormente");
			model.setViewName("redirect:/");
		}else {
			model.setViewName("login");
		}	
		
		if(error != null) {
			model.addObject("error", "Usuario o Password Incorrectos");	
		}
		
		if(logout != null) {
			model.addObject("exito", "Has cerrado sesion con exito");
		}
		return model;
	}

}
