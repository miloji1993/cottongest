package com.agroloji.cottongest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.agroloji.cottongest.dto.ClienteDto;
import com.agroloji.cottongest.dto.ParcelaDto;
import com.agroloji.cottongest.service.ClienteService;
import com.agroloji.cottongest.service.ParcelaService;

@Controller
@RequestMapping("/parcela")
@SessionAttributes("parcela")
public class ParcelaController {
	
	@Autowired
	private ParcelaService parcelaService;
	
	@Autowired
	private ClienteService clienteService;

	@GetMapping("/lista/{id}")
	public ModelAndView listar(@PathVariable("id") Integer idCliente) {
		ModelAndView model = new ModelAndView();
		List<ParcelaDto> parcelas = parcelaService.getAllByCliente(idCliente);
		ClienteDto cliente = clienteService.getById(idCliente);
		model.addObject("parcelas", parcelas);
		model.addObject("cliente", cliente);
		model.setViewName("/parcela/lista");
		return model;
	}
	
	@GetMapping("/insertar")
	public ModelAndView insertar(SessionStatus status) {
		ModelAndView model = new ModelAndView();
		List<ClienteDto> clientes = clienteService.getAll();
		ParcelaDto parcela = new ParcelaDto();
		model.addObject("parcela", parcela);
		model.addObject("clientes", clientes);
		model.addObject("titulo", "Insertar Parcela");
		model.setViewName("parcela/insertar");
		status.setComplete();
		return model;
	}
	
	
	@GetMapping("/editar/{id}")
	public ModelAndView editar(@PathVariable("id") Integer idParcela, RedirectAttributes mensaje) {
		ModelAndView model = new ModelAndView();
		ParcelaDto parcela = parcelaService.getById(idParcela);
		List<ClienteDto> clientes = clienteService.getAll();
		if(parcela != null) {
			model.addObject("parcela", parcela);
			model.addObject("clientes", clientes);
			model.addObject("titulo", "Editar Parcela");
			model.setViewName("/parcela/insertar");
		}else {
			mensaje.addFlashAttribute("error", "Fallo al encontrar la parcela");
			model.setViewName("redirect:/cliente/lista");
		}
		return model;
	}
	
	@GetMapping("/eliminar/{id}")
	public ModelAndView eliminar(@PathVariable("id") Integer idParcela, RedirectAttributes mensaje) {
		ModelAndView model = new ModelAndView("redirect:/cliente/lista");
		ParcelaDto parcelaDto = parcelaService.getById(idParcela);
		if(parcelaDto != null) {
			parcelaService.removeParcela(idParcela);
			mensaje.addFlashAttribute("exito", "Parcela eliminada con exito");
		}else {
			mensaje.addFlashAttribute("error", "Fallo al encontrar la parcela");
		}
		return model;
	}
	
	@PostMapping("/guardar")
	public ModelAndView guardar(@ModelAttribute() ParcelaDto parcela, RedirectAttributes mensaje) {
		ModelAndView model = new ModelAndView("redirect:/cliente/lista");
		Integer count = parcelaService.addParcela(parcela);
		if(count != 0) {
			mensaje.addFlashAttribute("exito", "Parcela guardado con exito");
		}else {
			mensaje.addFlashAttribute("error", "Fallo al guardar la parcela");
		}
		
		return model;
	}
}
