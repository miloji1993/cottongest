package com.agroloji.cottongest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.agroloji.cottongest.dto.MaquinariaDto;
import com.agroloji.cottongest.service.MaquinariaService;

@Controller
@RequestMapping("/maquinaria")
public class MaquinariaController {
	
	@Autowired
	private MaquinariaService maquinariaService;
	
	@GetMapping("/lista")
	public ModelAndView listado() {
		ModelAndView model = new ModelAndView();
		List<MaquinariaDto> maquinas = maquinariaService.getAll();
		model.addObject("maquinas", maquinas);
		model.setViewName("/maquinaria/lista");
		return model;
	}
	
	@GetMapping("/details/{id}")
	public ModelAndView detalles(@PathVariable("id") Integer idMaquinaria, RedirectAttributes mensaje) {
		ModelAndView model = new ModelAndView();
		MaquinariaDto maquinariaDto = maquinariaService.getById(idMaquinaria);
		if(maquinariaDto != null) {
			model.addObject("maquinaria", maquinariaDto);
			model.setViewName("/maquinaria/detalle");
		}else {
			mensaje.addFlashAttribute("error", "Fallo al encontrar maquinaria");
			model.setViewName("redirect:/maquinaria/lista");
		}
		return model;
	}
	
	@GetMapping("/insertar")
	public ModelAndView insertar() {
		MaquinariaDto maquinariaDto = new MaquinariaDto();
		ModelAndView model = new ModelAndView();
		model.addObject("maquinaria", maquinariaDto);
		model.addObject("titulo", "Insertar Maquinaria");
		model.setViewName("/maquinaria/insertar");
		return model;
	}
	
	@GetMapping("/editar/{id}")
	public ModelAndView editar(@PathVariable("id") Integer idMaquinaria, RedirectAttributes mensaje) {
		ModelAndView model = new ModelAndView();
		MaquinariaDto maquinaria = maquinariaService.getById(idMaquinaria);
		if(maquinaria != null) {
			model.addObject("maquinaria", maquinaria);
			model.addObject("titulo", "Editar Maquinaria");
			model.setViewName("/maquinaria/insertar");
		}else {
			mensaje.addFlashAttribute("error", "Fallo al encontrar maquinaria");
			model.setViewName("redirect:/maquinaria/lista");
		}
		return model;
	}
	
	@GetMapping("/eliminar/{id}")
	public ModelAndView eliminar(@PathVariable("id") Integer idMaquinaria, RedirectAttributes mensaje) {
		ModelAndView model = new ModelAndView("redirect:/maquinaria/lista");
		MaquinariaDto maquinariaDto = maquinariaService.getById(idMaquinaria);
		if(maquinariaDto != null) {
			maquinariaService.removeMaquinaria(idMaquinaria);
			mensaje.addFlashAttribute("exito", "Maquinaria eliminada con exito");
		}else {
			mensaje.addFlashAttribute("error", "Fallo al encontrar maquinaria");
		}
		return model;	
	}
	
	@PostMapping("/guardar")
	public ModelAndView guardar(@ModelAttribute() MaquinariaDto maquinaria, RedirectAttributes mensaje) {
		ModelAndView model = new ModelAndView("redirect:/maquinaria/lista");
		maquinariaService.addMaquinaria(maquinaria);
		mensaje.addFlashAttribute("exito", "Maquinaria guardado con exito");
		return model;
	}

}
