package com.agroloji.cottongest.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "destino")
@Data
public class Destino {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "iddestino")
	private Integer idDestino;
	
	@Column(name = "nombre", nullable = false)
	private String nombre;
	
	@Column(name = "poblacion", nullable = false)
	private String poblacion;
	
	@OneToMany(mappedBy = "destino", cascade = CascadeType.ALL)
	private List<Labor> labores;

}
