package com.agroloji.cottongest.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "cliente")
@Data
public class Cliente {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idcliente")
	private Integer idCliente;
	
	@Column(name = "nombre", nullable = false)
	private String nombre;
	
	@Column(name = "apellidos")
	private String apellidos;
	
	@Column(name = "dni_cif", nullable = false)
	private String dni;
	
	@Column(name = "direccion")
	private String direccion;
	
	@Column(name = "poblacion")
	private String poblacion;

	@Column(name = "provincia")
	private String provincia;
	
	@Column(name = "cp")
	private String cp;
	
	@Column(name = "telefono")
	private String telefono;
	
	@Column(name = "activo", nullable = false)
	private boolean activo;
	
	@Column(name = "alias")
	private String alias;
	
	@OneToMany(mappedBy = "cliente", cascade = CascadeType.ALL)
	private List<Parcela> parcelas;
}
