package com.agroloji.cottongest.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "maquinaria")
@Data
public class Maquinaria {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idmaquinaria")
	private Integer idMaquinaria;
	
	@Column(name = "nombre", nullable=false)
	private String nombre;
	
	@Column(name = "tipo", nullable=false)
	private String tipo;
	
	@Column(name = "modelo")
	private String modelo;
	
	@Column(name = "propia")
	private String propia;
	
	@Column(name = "matricula")
	private String matricula;
	
	@OneToMany(mappedBy = "maquinaria", cascade = CascadeType.ALL)
	private List<Labor> labores;
}
