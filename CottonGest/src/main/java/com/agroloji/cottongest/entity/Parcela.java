package com.agroloji.cottongest.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "parcela")
@Data
public class Parcela {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idparcela")
	private Integer idParcela;
	
	@Column(name = "nombre")
	private String nombre;

	@Column(name = "ha", nullable = false)
	private float ha;
	
	@ManyToOne(cascade = CascadeType.ALL, optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "idcliente", nullable = false)
	private Cliente cliente;
	
	@OneToMany(mappedBy = "parcela", cascade = CascadeType.ALL)
	private List<Labor> labores;
}
