package com.agroloji.cottongest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.agroloji.cottongest.entity.Destino;

@Repository
public interface DestinoRepository extends JpaRepository<Destino, Integer>{

}
