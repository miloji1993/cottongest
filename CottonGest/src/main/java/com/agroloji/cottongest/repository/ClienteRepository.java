package com.agroloji.cottongest.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.agroloji.cottongest.entity.Cliente;

@Repository
public interface ClienteRepository extends JpaRepository<Cliente, Integer>{

	@Modifying
	@Query("UPDATE Cliente c SET c.activo = :number Where c.idCliente = :id ")
	public void deleteCliente(@Param("number") boolean number ,@Param("id") Integer id);
	
	public List<Cliente> findByActivo(boolean activo);
}
