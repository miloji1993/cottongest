package com.agroloji.cottongest.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.agroloji.cottongest.entity.Cliente;
import com.agroloji.cottongest.entity.Parcela;

@Repository
public interface ParcelaRepository extends JpaRepository<Parcela, Integer> {
	
	public List<Parcela> findByCliente(Cliente cliente);
	
	@Modifying
	@Query("DELETE Parcela p WHERE p.idParcela = :id")
	public void deleteParcela(@Param("id") Integer idParcela);

}
