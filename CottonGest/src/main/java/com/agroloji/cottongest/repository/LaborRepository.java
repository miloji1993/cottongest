package com.agroloji.cottongest.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.agroloji.cottongest.entity.Labor;
import com.agroloji.cottongest.entity.Parcela;

@Repository
public interface LaborRepository extends PagingAndSortingRepository<Labor, Integer>{
	
	public List<Labor> findByFecha(Date fecha);
	
	public List<Labor> findByFechaOrParcela(Date fecha, Parcela parcela);

}
