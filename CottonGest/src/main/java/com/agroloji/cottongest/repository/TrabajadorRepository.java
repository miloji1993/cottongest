package com.agroloji.cottongest.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.agroloji.cottongest.entity.Trabajador;

@Repository
public interface TrabajadorRepository extends JpaRepository<Trabajador, Integer>{
	
	@Modifying
	@Query("UPDATE Trabajador t SET t.activo = :number Where t.idTrabajador = :id ")
	public void deleteTrabajador(@Param("number") boolean number ,@Param("id") Integer id);
	
	public List<Trabajador> findByActivo(boolean activo);

}
