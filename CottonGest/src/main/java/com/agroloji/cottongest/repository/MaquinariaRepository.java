package com.agroloji.cottongest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.agroloji.cottongest.entity.Maquinaria;

@Repository
public interface MaquinariaRepository extends JpaRepository<Maquinaria, Integer>{

}
