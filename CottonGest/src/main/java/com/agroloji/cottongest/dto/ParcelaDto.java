package com.agroloji.cottongest.dto;

import java.io.Serializable;
import java.util.List;

import com.agroloji.cottongest.entity.Cliente;
import com.agroloji.cottongest.entity.Labor;

import lombok.Data;

@Data
public class ParcelaDto implements Serializable{

	private static final long serialVersionUID = 1L;

	private Integer idParcela;
	
	private String nombre;

	private float ha;
	
	private Cliente cliente;
	
	private List<Labor> labores;
}
