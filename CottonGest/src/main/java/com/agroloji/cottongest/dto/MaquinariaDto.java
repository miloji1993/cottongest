package com.agroloji.cottongest.dto;

import java.io.Serializable;
import java.util.List;

import com.agroloji.cottongest.entity.Labor;

import lombok.Data;

@Data
public class MaquinariaDto implements Serializable{
	
	private static final long serialVersionUID = 1L;

	private Integer idMaquinaria;
	
	private String nombre;
	
	private String tipo;
	
	private String modelo;
	
	private String propia;
	
	private String matricula;
	
	private List<Labor> labores;

}
