package com.agroloji.cottongest.dto;

import java.io.Serializable;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.agroloji.cottongest.entity.Destino;
import com.agroloji.cottongest.entity.Maquinaria;
import com.agroloji.cottongest.entity.Parcela;
import com.agroloji.cottongest.entity.Trabajador;

import lombok.Data;

@Data
public class LaborDto implements Serializable{
	
	private static final long serialVersionUID = 1L;

	private Integer idLabor;
	
	private double kilos;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date fecha;

	private double horas;
	
	private String descripcion;
	
	@DateTimeFormat(pattern = "HH:mm")
	private Date entrada;
	
	@DateTimeFormat(pattern = "HH:mm")
	private Date salida;
	
	private Parcela parcela;
	
	private Destino destino;
	
	private Trabajador trabajador;
	
	private Maquinaria maquinaria;

}


