package com.agroloji.cottongest.dto;

import java.io.Serializable;
import java.util.List;

import com.agroloji.cottongest.entity.Labor;

import lombok.Data;

@Data
public class TrabajadorDto implements Serializable{
	
	private static final long serialVersionUID = 1L;

	private Integer idTrabajador;
	
	private String nombre;
	
	private String apellidos;

	private String dni;
	
	private String direccion;
	
	private String poblacion;
	
	private String provincia;
	
	private String cp;
	
	private String telefono;
	
	private boolean activo;
	
	private String tipo;
	
	private String externo;
	
	private List<Labor> labores;

}
