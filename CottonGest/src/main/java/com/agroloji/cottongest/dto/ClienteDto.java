package com.agroloji.cottongest.dto;

import java.io.Serializable;
import java.util.List;

import com.agroloji.cottongest.entity.Parcela;

import lombok.Data;

@Data
public class ClienteDto implements Serializable{

	private static final long serialVersionUID = 1L;

	private Integer idCliente;
	
	private String nombre;
	
	private String apellidos;
	
	private String dni;

	private String direccion;
	
	private String poblacion;

	private String provincia;
	
	private String cp;
	
	private String telefono;
	
	private boolean activo;
	
	private String alias;
	
	private List<Parcela> parcelas;
}
