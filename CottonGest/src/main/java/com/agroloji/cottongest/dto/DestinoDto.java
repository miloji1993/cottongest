package com.agroloji.cottongest.dto;

import java.io.Serializable;
import java.util.List;

import com.agroloji.cottongest.entity.Labor;

import lombok.Data;

@Data
public class DestinoDto implements Serializable{
	
	private static final long serialVersionUID = 1L;

	private Integer idDestino;
	
	private String nombre;
	
	private String poblacion;
	
	private List<Labor> labores;

}
